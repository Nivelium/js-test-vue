# js-test-vue

> A Vue.js project

## После того, как склонировали репо, выполнить следующие команды:

``` bash

npm install
npm run dev

```

Результат будет на localhost:8080/?word=YourAwesomeWord

Задание 4 находится по path: ./src/helpers/StringManipulator

Также есть линтер(выполнять только после npm install): npm run lint.
