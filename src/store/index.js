import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const state = {
  currentText: ' ',
  counter: null,
};

const getters = {
  getCounter (state) {
    return state.counter;
  },
  getCurrentText (state) {
    return state.currentText;
  }
};

const actions = {

};

const mutations = {
  incrementCounter (state) {
    if (state.counter === null) {
      return state.counter = 0;
    }
    state.counter = (state.counter + 1) % state.currentText.length;
  },
  setCurrentText (state, text) {
    state.currentText = text;
  }
};

export default new Vuex.Store({
  modules: {
  
  },
  state,
  getters,
  actions,
  mutations
})
