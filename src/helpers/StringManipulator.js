class StringManipulator {
  constructor(str) {
    this.str = str;
  }
  upper() {
    this.str = this.str.toUpperCase();
    return this;
  }
  reverse() {
    this.str = this.str.split("").reverse().join("");
    return this;
  }
}

export default StringManipulator;
